<?php

namespace DL\ContactFormBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dl_contact_form');

        $rootNode
            ->children()
                ->scalarNode('contact_to')
                    ->defaultValue('support@dotlabel.co.uk')
                ->end()
                ->scalarNode('contact_from')
                    ->defaultValue('no-reply@dotlabel.co.uk')
                ->end()
                ->scalarNode('contact_subject')
                    ->defaultValue('Unconfigured Contact Subject')
                ->end()
                ->scalarNode('contact_company_email_template')
                    ->defaultValue('DLContactForm:toCompany.html.twig')
                ->end() 
                ->scalarNode('contact_customer_email_template')
                    ->defaultValue(null)
                ->end();

        return $treeBuilder;
    }
}
