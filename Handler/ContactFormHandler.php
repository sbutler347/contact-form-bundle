<?php

namespace DL\ContactFormBundle\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use DL\CoreBundle\Http\Response\JsonSuccessResponse;
use DL\CoreBundle\Http\Response\JsonFormErrorsResponse;
use DL\CoreBundle\Http\Response\JsonErrorResponse;

class ContactFormHandler {

    private $em;
    private $dlEmailer;
    private $contactTo;
    private $contactFrom;
    private $contactSubject;
    private $companyEmailTemplate;
    private $customerEmailTemplate;
    private $nonAjaxFormSubmitted;
	private $twig;

    public function __construct(EntityManager $em, $dlEmailer, $twig, $contactTo, $contactFrom, $contactSubject, $companyEmailTemplate, $customerEmailTemplate=null)
    {
        $this->em                       = $em;
        $this->dlEmailer                = $dlEmailer;
		$this->twig						= clone $twig;
        $this->contactTo                = $contactTo;
        $this->contactFrom              = $contactFrom;
        $this->contactSubject           = $contactSubject;
        $this->companyEmailTemplate     = $companyEmailTemplate;
        $this->customerEmailTemplate    = $customerEmailTemplate;

        $this->nonAjaxFormSubmitted     = false;
    }

    public function isNonAjaxFormSubmitted()
    {
        return $this->nonAjaxFormSubmitted === true;
    }

    public function handle(FormInterface $form, Request $request, $page)
    {
        // dont get involved unless this is a form submit
        if (false === $request->isMethod('POST')) {
            return false;
        }

        // track non ajax success
        $this->nonAjaxFormSubmitted = false;

        // handle the form
        $form->handleRequest($request);

        /**
         * If the form has been posted and is validated
         */
        if ($form->isValid()) {

			$entity = $form->getData();

            // save to the database
            $this->em->persist($entity);
            $this->em->flush();

            // send the contact email
            $this->emailCompany($entity);

            // let the customer know
            if (null !== $this->customerEmailTemplate) {
                $this->emailCustomer($page, $entity);
            }

            if ($request->isXmlHttpRequest()) {
                return new JsonSuccessResponse();
            }
        }

        // if the post is ajax
        if ($form->isSubmitted() && false === $form->isValid()) {

            if ($request->isXmlHttpRequest()) {
                return new JsonFormErrorsResponse($form);
            }

            // if the form was not ajax but had errors they will bound to the form
        }

        // 
        $this->nonAjaxFormSubmitted = true;
    }

    /**
     * The email to Company
     *
     * @param ContactEnquiry $entity
     */
    public function emailCompany($entity)
    {
        // the message config
        $emailParams = array(
            'title' => $this->contactSubject,
            'entity' => $entity,
            'includeIP' => true,
        );

        //
        $this->dlEmailer->sendRenderedEmail(
            $this->contactTo,
            $entity->getEmail(),
            $this->contactSubject,
            $emailParams,
            $this->companyEmailTemplate
        );
    }

    /**
     * Email the customer
     */
    public function emailCustomer($page, $entity)
    {
        // get the content repo
        $repo = $this->em->getRepository('DLPageBundle:PageContentPanel');

        // we want to tell them they they are now registered and send them an email
        $customerEmailContentHtml = $repo->getPublishedContentForPageByName($page, 'Customer Confirmation Email');

        $twigString = $this->twig;
        $twigString->setLoader(new \Twig_Loader_String());

        $customerContentHtml = $twigString->render($customerEmailContentHtml, array('entity'=>$entity));

        // create another email and send it to the person getting in touch
        $emailParams = array(
            'title' => $this->contactSubject,
            'content'=>$customerContentHtml,
			'entity' =>$entity
        );

        //
        $this->dlEmailer->sendRenderedEmail(
            $entity->getEmail(),
            $this->contactFrom,
            $this->contactSubject,
            $emailParams,
            $this->customerEmailTemplate
        );
    }
}
